# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: MIT

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

# git binary diffs are not supported when using the default patch
PATCHTOOL = "git"

SRC_URI += "file://0001-subset-Remove-Franklin-from-the-tests-which-is-not-a.patch"
